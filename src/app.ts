import "@babylonjs/core/Debug/debugLayer";
import "@babylonjs/inspector";
import "@babylonjs/loaders/glTF";
import {
  AbstractMesh,
  ArcRotateCamera,
  BaseTexture,
  Color3,
  Color4,
  CubeTexture,
  Engine,
  EngineFactory,
  EnvironmentHelper,
  GroundMesh,
  HemisphericLight,
  Mesh,
  MeshBuilder,
  Nullable,
  PBRMaterial,
  PerformanceMonitor,
  Scene,
  SceneInstrumentation,
  StandardMaterial,
  Texture,
  Vector3,
  VideoTexture
} from "@babylonjs/core";

class App {

  private _canvas: HTMLCanvasElement;
  private _scene: Scene;
  private _engine: Engine;
  private _env: Nullable<EnvironmentHelper> = null;


  constructor() {
    this._canvas = this._createCanvas();

    // initialize babylon scene and engine
    this._init();

  }


  private async _init(): Promise<void> {
    this._engine = (await EngineFactory.CreateAsync(this._canvas, undefined)) as Engine;
    this._scene = new Scene(this._engine);

    // read environment and load into the scene for VR/XR 
    this._env = this._scene.createDefaultEnvironment();
    // add XR support
    const xr = await this._scene.createDefaultXRExperienceAsync({
      //floorMeshes: [this._env.ground] ,
    });

    //**for development: make inspector visible/invisible
    window.addEventListener("keydown", (ev) => {
      //Shift+Ctrl+D
      if (ev.shiftKey && ev.ctrlKey && ev.key === 'D') {
        if (this._scene.debugLayer.isVisible()) {
          this._scene.debugLayer.hide();
        } else {
          this._scene.debugLayer.show();
        }
      }
    });


    //MAIN render loop & state machine
    await this._main();
  }

  //set up the canvas
  private _createCanvas(): HTMLCanvasElement {

    //Commented out for development
    document.documentElement.style["overflow"] = "hidden";
    document.documentElement.style.overflow = "hidden";
    document.documentElement.style.width = "100%";
    document.documentElement.style.height = "100%";
    document.documentElement.style.margin = "0";
    document.documentElement.style.padding = "0";
    document.body.style.overflow = "hidden";
    document.body.style.width = "100%";
    document.body.style.height = "100%";
    document.body.style.margin = "0";
    document.body.style.padding = "0";

    //create the canvas html element and attach it to the webpage
    this._canvas = document.createElement("canvas");
    this._canvas.style.width = "100%";
    this._canvas.style.height = "100%";
    this._canvas.id = "myBjsCanvas";
    

    document.body.appendChild(this._canvas);

    return this._canvas;
  }


  private async _main(): Promise<void> {

    // Action goes HERE!
    // this._test();
    this._materials_colors();

    this._engine.runRenderLoop(() => {
      this._scene.render();
    });
    
    //resize if the screen is resized/rotated
    window.addEventListener('resize', () => {
      this._engine.resize();
    });

  }


  private _test(): void {

    var camera: ArcRotateCamera = new ArcRotateCamera(
      "Camera", 
      Math.PI / 2, 
      Math.PI / 3, 
      2,
      Vector3.Zero(), 
      this._scene
    );
    camera.attachControl(this._canvas, true);

    var light1: HemisphericLight = new HemisphericLight(
      "light1", 
      new Vector3(1, 1, 0), 
      this._scene
    );
    light1.diffuse = new Color3(0.2,0.3,0.4);

    var sphere: Mesh = MeshBuilder.CreateSphere(
      "sphere", 
      { diameter: 1 }, 
      this._scene
    );

    var ground: GroundMesh = MeshBuilder.CreateGround(
      "ground", 
      {height: 3, width: 3, subdivisions: 1},
      this._scene
    );

    // background
    // this._scene.clearColor = new Color4(0.4,0.5,0.4,1);

  }



  private _materials_colors(): void {

    // background
    //this._scene.clearColor = new Color4(0.4,0.5,0.4,1);
    // ambient
    //this._scene.ambientColor = new Color3(0, 0, 1);

    // Scene: Collision system enabled
    this._scene.collisionsEnabled = true;

    // Scene: gravity
    const assumedFramesPerSecond = 60;
    const earthGravity = -9.81;
    this._scene.gravity = new Vector3(0, earthGravity / assumedFramesPerSecond, 0);

    var camera: ArcRotateCamera = new ArcRotateCamera(
      "Camera", 
      4, // alpha 
      1.3, // beta
      3, // radius
      new Vector3(-0.9, 0.8, -2), 
      this._scene
    );
    camera.attachControl(this._canvas, true);
    // can't penetrate anything with this same property set to true
    camera.checkCollisions = true;
    // let's say I cannot fly
    camera


    var light1: HemisphericLight = new HemisphericLight(
      "light1", 
      new Vector3(10, 10, 0), 
      this._scene
    );
    light1.diffuse = new Color3(0.5, 0.5, 0.5);


    //Skybox
    const skybox = MeshBuilder.CreateBox("skybox1", {size:18}, this._scene);
    const skyboxMaterial = new StandardMaterial("skybox1", this._scene);
    skyboxMaterial.backFaceCulling = false;
    skyboxMaterial.reflectionTexture = new CubeTexture("media/skybox1/skybox", this._scene);
    skyboxMaterial.reflectionTexture.coordinatesMode = Texture.SKYBOX_MODE;
    skyboxMaterial.diffuseColor = new Color3(0, 0, 0);
    skyboxMaterial.specularColor = new Color3(0, 0, 0);
    skybox.material = skyboxMaterial;


    var sphere: Mesh = MeshBuilder.CreateSphere(
      "sphere", 
      { diameter: 1 }, 
      this._scene
    );
    sphere.checkCollisions = true;
    var material_1 = new StandardMaterial("material_1", this._scene);
    //material_1.ambientColor = new Color3(1, 0, 0);
    //material_1.diffuseColor = new Color3(1, 0, 0);
    //material_1.specularColor = new Color3(0, 0, 0.5);
    //material_1.emissiveColor = new Color3(1, 1, 1);
    //material_1.alpha = 0.5;
    var texture_1 = new Texture('media/text7.png', this._scene);
    //var texture = new Texture('media/death_star.png', this._scene);
    material_1.diffuseTexture = texture_1;
    //material_1.diffuseTexture.hasAlpha = true;
    //material_1.bumpTexture = new Texture("media/normalMap.jpg")
    //material_1.diffuseTexture = new VideoTexture("video", "media/clouds.mp4", this._scene);
    sphere.material = material_1;


    var options = {width:21, height :21, subdivisions: 100, minHeight: 0, maxHeight: 1};
    var ground = MeshBuilder.CreateGroundFromHeightMap(
      'gdhm', 
      'media/heightmap2.png', 
      options , 
      this._scene,
    );
    ground.checkCollisions = true;
    var material_2 = new StandardMaterial("material_2", this._scene);
    var texture_2 = new Texture('media/text6.png', this._scene);
    material_2.diffuseTexture = texture_2;
    ground.material = material_2;


    // moving the textures
    this._scene.registerBeforeRender(() => {
      texture_1.uOffset += 0.001;
      //texture.vOffset += 0.001;
      //texture.uScale += 0.01;
      texture_2.uOffset += 0.0005;
      // FPS
      //console.log(this._engine.getFps());
    });


    


  }


}
new App();